#! /bin/sh
# Copyright (c) 2020, 2023 D. Bohdan.
# License: MIT.

set -e

. ~/.profile

cd "$(dirname "$(readlink -f "$0")")"/..
poe test
