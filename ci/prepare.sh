#! /bin/sh
# Copyright (c) 2020, 2023 D. Bohdan.
# License: MIT.

set -e

apt-get update
apt-get install -y borgbackup pipx rsync tox

pipx ensurepath

for pkg in poethepoet poetry; do
    pipx install "$pkg"
done
