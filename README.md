# queen

Control [BorgBackup](https://www.borgbackup.org/) to create, prune, and upload directory backups in a single command with sensible defaults.


## Requirements

* BorgBackup 1.1.4+
* Python 3.8+
* Optional:
    * Rclone
    * rsync

```sh
# Debian/Ubuntu
sudo apt install borgbackup
```


## Usage

### Initialization

```sh
BORG_PASSPHRASE=passphrase borg init /backup/foo -e repokey-blake2
```

### crontab line

```crontab
# Back up ~/foo/ to /backup/foo/ hourly.
# Rclone and rsync the repository to a remote location.
0 * * * * BORG_PASSPHRASE=passphrase queen --repo /backup/foo --source ~/foo --exclude foo/.stversions --exclude foo/tmp --rclone-to b2:/backup/foo --rsync-to vps:~/backup/foo --rsync-args '--bwlimit 2M --rsh "ssh -p 2222"'
```


## Similar projects

* [Borg wrappers](https://github.com/borgbackup/community#backup-scripts--borg-wrappers)&thinsp;&mdash;&thinsp;a list in the Borg Community repository
* [Locutus](https://github.com/janost/locutus)


## License

MIT.
