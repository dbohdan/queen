#! /usr/bin/env python3
# queen -- control BorgBackup to create, prune, and upload directory backups
# in a single command with sensible defaults.
# Copyright (c) 2019-2020, 2022-2024 D. Bohdan.
# License: MIT.

from __future__ import annotations

import argparse
import logging
import os
import shlex
import subprocess as sp
import sys
import time
from pathlib import Path
from typing import Sequence


def main() -> None:
    config = parse_command_line(sys.argv[1:])
    set_up_logging(verbose=config.verbose)

    name = config.name
    if not name:
        name = config.source.name

    if "BORG_PASSPHRASE" not in os.environ:
        logging.error("BORG_PASSPHRASE not set")
        sys.exit(1)

    os.environ.setdefault("BORG_RELOCATED_REPO_ACCESS_IS_OK", "no")
    os.environ.setdefault("BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK", "no")

    create(
        config.repo,
        f"{name}-{time.strftime('%Y%m%d-%H%M')}",
        config.source,
        borg_command=config.borg_command,
        compression=config.compression,
        excludes=config.excludes,
        verbose=config.verbose,
    )

    prune(
        config.repo,
        f"{name}-*",
        borg_command=config.borg_command,
        verbose=config.verbose,
    )

    if config.rclone_to:
        rclone(
            config.repo,
            config.rclone_to,
            shlex.split(config.rclone_args),
            verbose=config.verbose,
        )

    if config.rsync_to:
        rsync(
            config.repo,
            config.rsync_to,
            shlex.split(config.rsync_args),
            verbose=config.verbose,
        )


def create(
    repo: Path,
    template: str,
    source: Path,
    *,
    borg_command: str,
    compression: str,
    excludes: Sequence[str],
    verbose: bool,
) -> None:
    os.chdir(source.parent)
    logging.info("CWD is %s", quote(Path().absolute()))

    source_name = source.name
    logging.info("creating archive of %s in repo %s", quote(source_name), quote(repo))

    exclude_args = [x for exclude in excludes for x in ("--exclude", exclude)]

    args: list[Path | str] = [
        "create",
        f"{repo}::{template}",
        source_name,
        "--compression",
        compression,
        *exclude_args,
        "--exclude-caches",
        "--exclude-if-present",
        ".nobackup",
        "--exclude-nodump",
    ]

    borg(args, borg_command=borg_command, verbose=verbose)


def prune(
    repo: Path,
    glob: str,
    hourly: int = 168,
    daily: int = 31,
    weekly: int = 12,
    monthly: int = -1,
    *,
    borg_command: str,
    verbose: bool,
) -> None:
    logging.info("pruning %s in repo %s", quote(glob), quote(repo))

    args = [
        "prune",
        repo,
        "--glob-archives",
        glob,
        f"--keep-hourly={hourly}",
        f"--keep-daily={daily}",
        f"--keep-weekly={weekly}",
        f"--keep-monthly={monthly}",
    ]

    borg(args, borg_command=borg_command, verbose=verbose)


def rclone(repo: Path, dest: str, arguments: Sequence[str], *, verbose: bool) -> None:
    logging.info("rcloning %s to %s", quote(repo), quote(dest))

    if verbose:
        arguments = ["--progress", *arguments]

    command = [
        "rclone",
        "sync",
        "--modify-window",
        "1s",
        *arguments,
        repo,
        dest,
    ]

    run_command(command)


def rsync(repo: Path, dest: str, arguments: Sequence[str], *, verbose: bool) -> None:
    logging.info("rsyncing %s to %s", quote(repo), quote(dest))

    if verbose:
        arguments = ["--progress", "--verbose", *arguments]

    command = [
        "rsync",
        "--archive",
        "--compress",
        *arguments,
        f"{repo}/",
        dest,
    ]

    run_command(command)


def borg(args: Sequence[Path | str], *, borg_command: str, verbose: bool) -> None:
    command = [borg_command, *args]
    if verbose:
        command.extend(["-v", "--stats"])

    run_command(command)


def run_command(command: Sequence[str]) -> None:
    result = sp.run(
        command,
        check=False,
    )

    if result.returncode != 0:
        logging.error(
            "command exited with code %d: %s",
            result.returncode,
            shlex.join(command),
        )
        sys.exit(1)


def quote(path: Path | str) -> str:
    s = shlex.quote(str(path))
    if not s.startswith("'"):
        s = f"'{s}'"

    return s


def parse_command_line(argv: Sequence[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description=(
            "Control BorgBackup to create, prune, and upload directory backups "
            "in a single command with sensible defaults."
        ),
    )

    parser.add_argument(
        "--repo",
        metavar="PATH",
        required=True,
        type=Path,
        help="path to the BorgBackup repository",
    )

    parser.add_argument(
        "--source",
        metavar="PATH",
        required=True,
        type=Path,
        help="source directory to back up",
    )

    parser.add_argument(
        "--borg-command",
        default="borg",
        metavar="COMMAND",
        help='BorgBackup command (default: "%(default)s")',
    )

    parser.add_argument(
        "--compression",
        default="auto,zstd,7",
        metavar="OPTS",
        help='compression options (default: "%(default)s")',
    )

    parser.add_argument(
        "--exclude",
        action="append",
        default=[],
        dest="excludes",
        metavar="PATH",
        help="paths to exclude from the backup relative to the parent of the source",
    )

    parser.add_argument("--name", default="", help="name for the backup archive")

    parser.add_argument(
        "--rclone-to",
        default="",
        metavar="DEST",
        help="Rclone destination for backup sync",
    )

    parser.add_argument(
        "--rclone-args",
        default="",
        metavar="ARGS",
        help="additional arguments for Rclone",
    )

    parser.add_argument(
        "--rsync-to",
        default="",
        metavar="DEST",
        help="rsync destination for backup sync",
    )

    parser.add_argument(
        "--rsync-args",
        default="",
        metavar="ARGS",
        help="additional arguments for rsync",
    )

    parser.add_argument("--verbose", action="store_true", help="enable verbose logging")

    args = parser.parse_args(argv)
    if args.source.is_symlink():
        parser.error(f"source is a symlink: {quote(args.source)}")

    return args


def set_up_logging(*, verbose: bool) -> None:
    logging.basicConfig(
        datefmt="%Y-%m-%d %H:%M:%S %z",
        format="== {asctime} ({levelname}) {message}",
        level=logging.INFO if verbose else logging.WARNING,
        style="{",
    )


if __name__ == "__main__":
    main()
