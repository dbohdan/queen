#! /usr/bin/env python3

from __future__ import annotations

import os
import secrets
import shlex
import subprocess as sp
import sys
import tempfile
import unittest
from pathlib import Path

QUEEN_COMMAND = [sys.executable, "queen.py"]


def run(*args: Path | str) -> str:
    print(f"===== {shlex.join(str(x) for x in args)}")

    output = ""
    try:
        result = sp.run(
            args,
            capture_output=True,
            check=False,
            text=True,
        )

        output = result.stderr + result.stdout

        if result.returncode != 0:
            raise sp.CalledProcessError(
                cmd=args, returncode=result.returncode, output=output
            )

        return output
    finally:
        print(output)


class TestQueen(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        os.chdir(Path(__file__).parent.parent)

        cls.temp_dir = tempfile.TemporaryDirectory()
        cls.temp_dir_path = Path(cls.temp_dir.name)

        cls.repo = cls.temp_dir_path / "repo"
        cls.sync_dest = cls.temp_dir_path / "sync-dest"

        cls.passphrase = secrets.token_urlsafe(15)
        os.environ["BORG_PASSPHRASE"] = cls.passphrase

        run("borg", "init", "-e", "repokey-blake2", cls.repo)

    @classmethod
    def tearDownClass(cls):
        cls.temp_dir.cleanup()

    def test_create_backup(self):
        run(
            *QUEEN_COMMAND,
            "--borg-command",
            "borg",
            "--name",
            "this-is-a-test",
            "--repo",
            self.repo,
            "--source",
            self.temp_dir_path,
        )

    def test_create_backup_and_rsync(self):
        run(
            *QUEEN_COMMAND,
            "--borg-command",
            "borg",
            "--compression",
            "zstd",
            "--name",
            "this-is-another-test",
            "--repo",
            self.repo,
            "--rsync-to",
            self.sync_dest,
            "--source",
            self.temp_dir_path,
            "--verbose",
        )

    def test_list_backups(self):
        result = run("borg", "list", self.repo)
        self.assertRegex(result, r"this-is-a-test-\d{4}")

    def test_list_backups_after_rsync(self):
        os.environ["BORG_RELOCATED_REPO_ACCESS_IS_OK"] = "yes"
        result = run(
            "borg",
            "list",
            self.sync_dest,
        )
        self.assertRegex(result, r"this-is-another-test-\d{4}")


if __name__ == "__main__":
    unittest.main()
